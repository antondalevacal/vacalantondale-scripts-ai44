#! /bin/bash

read -p "Please enter your last name: " LNAME
read -p "Please enter your first name: " FNAME
read -p "Please enter the year of your birthday: " YEAR 
read -p "Please enter the month of your birthday in numerical format (i.e. 01,05,etc.): " MONTH 
read -p "Please enter the day of your birthday: " DAY

CURRENTYEAR=$(date '+%Y')
CURRENTMONTH=$(date '+%m')

YEARAGE=$(($CURRENTYEAR - $YEAR))
MONTHAGE=$(($CURRENTMONTH - $MONTH))

if [ $MONTHAGE -lt 0 ] ; then
    MONTHAGE=$(($CURRENTMONTH-$MONTH+12))
else
    MONTHAGE=$(($CURRENTMONTH-$MONTH))
fi
echo "Hello, $FNAME $LNAME! You're $YEARAGE years old!"