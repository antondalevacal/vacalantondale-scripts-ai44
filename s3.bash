#!/bin/bash
CONST=1
read -p "Please enter the name of the folder/s: " FOLNAME
read -p "Please enter the number of folders to be created: " FOLNUM
if [ $FOLNUM = 1 ]
    then 
        mkdir $FOLNAME
elif [ $FOLNUM -gt 1 ]
    then
        mkdir $FOLNAME
        until [ $CONST -ge $FOLNUM ]
            do
                CONST=$(($CONST+1))
                mkdir $FOLNAME$CONST
        done
fi